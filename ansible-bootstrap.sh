#!/bin/bash

local=false
display_help() {
  echo "Usage: ansible-bootstrap [option...] {local|remote|help}" >&2
  echo
  echo "   -l, --local, local           set the connection: local in the main playbook"
  echo "   -r, --remote, remote         [default] connection: local is removed, forcing ssh connection to the target machine"
  echo "   -h, --help, help             display this pretty message"
  echo
  exit 1
}

validate_bootstrap() {
  echo "Directory not empty. Are you sure you want to bootstrap your ansible work directory again ?"
  read -p "--- main.yml, inventory.hosts and ansible.cfg will be re-written --- [y/N] ?" -n 1 -r choice
  echo
  case "$choice" in 
    y|Y ) echo "bootstraping...";;
    n|N ) echo "Exiting, no changes..."
          exit
          ;;
    * )   echo "Exiting, no changes..."
          exit
          ;;
esac
}

while [ "$1" != "" ]; do
    case $1 in
        -l | --local | local )    local=true
                                  ;;
        -e | --remote | remote )  local=false
                                  ;;
        -h | --help | help )      display_help
                                  exit
                                  ;;
        * )                       local=false
                                  ;;
    esac
    shift
done

FILE=./main.yml
if test -f "$FILE"; then
  validate_bootstrap
fi

# Ansible Config file
cat << EOF > ./ansible.cfg
[defaults]
host_key_checking = False
inventory=./inventory.hosts
retry_files_enabled = False
nocolor = 0
force_color = 1
stdout_callback = debug
EOF

# Inventory File
cat << EOF > ./inventory.hosts
[localhost]
127.0.0.1
EOF

# Main playbook file
cat << EOF > ./main.yml
---
- hosts: localhost
EOF

if [ "$local" = true ] ; then
  echo "  connection: local" >> ./main.yml
fi

cat << EOF >> ./main.yml
  tasks:
    - name: Run Playbook
      debug:
        msg: "Running tasks..."

EOF

mkdir group_vars 2> /dev/null
mkdir templates 2> /dev/null
mkdir roles 2> /dev/null

cat << EOF > ./group_vars/all.yml
---
EOF

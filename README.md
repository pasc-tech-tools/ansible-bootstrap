# ansible-bootstrap

Bash script for bootstrap ansible directory for the control machine.

## Set Up

Prepare the directory where you'll keep the bash script. Example :

```
mkdir ~/tools && cd ~/tools
git clone https://gitlab.com/pasc-tech-tools/ansible-bootstrap.git
chmod 744 ansible-bootstrap/ansible-bootstrap.sh
```

Create an alias to set up your `ansible-bootstrap` command. Point it to where the script has been downloaded. Example :

```
cat << EOF >> ~/.zshrc
alias ansible-bootstrap="~/tools/ansible-bootstrap/ansible-bootstrap.sh"
source ~/.zshrc
EOF
```

## Usage

Create your working directory and type in the `ansible-bootstrap` command :

```
mkdir ~/workspace && cd ~/workspace
ansible-bootstrap
```

The script should set up your ansible project on your control machine like this :

```
tree
.
├── ansible.cfg
├── group_vars
│   └── all.yml
├── inventory.hosts
├── main.yml
├── roles
└── templates
```

## Options

### remote connection (default)

This is the default connection config. In the `main.yml` file, you should see the default hosts configuration :

```
---
- hosts: localhost
  tasks:
[...]
```

Notice no connection parameter is define, meaning it will use ssh connection to connect to the target machine in this case, `localhost`.

### local connection

Running the local option will configure the host configuration with the local connection.
This will allow the connection to the target machine to be done through the local interface, meaning no ssh connection required. Usage :

```
ansible-bootstrap -l | --local | local
```

Opening the `main.yml` file now :

```
---
- hosts: localhost
  connection: local
  tasks:
[...]
```

### help

Fancy option to pretty print the usage of the `ansible-bootstrap` command. Usage :

```
ansible-bootstrap -h | --help | help
```


## Comming Soon

### Option for Docker connection